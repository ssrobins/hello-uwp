@echo off
setlocal

set build_dir=build_uwp

cd /d %~dp0
if not exist %build_dir% mkdir %build_dir%
cd %build_dir%

cmake -G "Visual Studio 16 2019" -A x64 -DCMAKE_SYSTEM_NAME=WindowsStore -DCMAKE_SYSTEM_VERSION=10.0 --debug-trycompile .. || goto :error

set config=Release

cmake --build . --config %config% -- /m || goto :error

:error
exit /b %errorlevel%
